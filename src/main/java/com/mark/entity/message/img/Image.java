/**
 * @file Image.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.img;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * image对象
 * @author maliqiang
 * @create 2017-08-03
 * @version 1.0
 */
public class Image {
    @XStreamAlias("MediaId")
    private String mediaId;//多媒体资源ID，多媒体消息才有
    public Image(){}

    public Image(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
