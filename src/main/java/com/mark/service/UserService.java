package com.mark.service;

import com.mark.entity.User;

import java.util.List;

public interface UserService{

    public int insert(User pojo);

    public int insertSelective(User pojo);

    public int insertList(List<User> pojos);

    public int update(User pojo);

    public User getByUserName(String userName);
}
