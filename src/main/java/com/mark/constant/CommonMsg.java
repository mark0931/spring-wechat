/**
 * @file CommonMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.constant;

/**
 * 常用对话
 * @author maliqiang
 * @create 2017-07-26
 * @version 1.0
 */
public class CommonMsg {
    public static final String HELP_DESC = "欢迎关注马里奥的织梦园！";
}
