/**
 * @file MessageUtil.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.util;

import com.mark.entity.message.img.Image;
import com.mark.entity.message.img.ImgReqMsg;
import com.mark.entity.message.img.ImgRspMsg;
import com.mark.entity.message.text.TextReqMsg;
import com.mark.entity.message.text.TextRspMsg;
import com.mark.entity.message.voice.Voice;
import com.mark.entity.message.voice.VoiceReqMsg;
import com.mark.entity.message.voice.VoiceRsqMsg;

/**
 * 消息构造工具类
 * 具体被动消息回复体请看微信官方文档，地址请看@link
 *
 * @author maliqiang
 * @version 1.0
 * @create 2017-07-28
 * @link https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140543
 */
public class MessageUtil {

    /**
     * 返回消息类型：文本
     */
    public static final String RESP_MESSAGE_TYPE_TEXT = "text";

    /**
     * 返回消息类型：音乐
     */
    public static final String RESP_MESSAGE_TYPE_MUSIC = "music";

    /**
     * 返回消息类型：图文
     */
    public static final String RESP_MESSAGE_TYPE_NEWS = "news";

    /**
     * 请求消息类型：文本
     */
    public static final String REQ_MESSAGE_TYPE_TEXT = "text";

    /**
     * 请求消息类型：图片
     */
    public static final String REQ_MESSAGE_TYPE_IMAGE = "image";

    /**
     * 请求消息类型：链接
     */
    public static final String REQ_MESSAGE_TYPE_LINK = "link";

    /**
     * 请求消息类型：地理位置
     */
    public static final String REQ_MESSAGE_TYPE_LOCATION = "location";

    /**
     * 请求消息类型：音频
     */
    public static final String REQ_MESSAGE_TYPE_VOICE = "voice";

    /**
     * 请求消息类型：推送
     */
    public static final String REQ_MESSAGE_TYPE_EVENT = "event";

    /**
     * 事件类型：subscribe(订阅)
     */
    public static final String EVENT_TYPE_SUBSCRIBE = "subscribe";

    /**
     * 事件类型：unsubscribe(取消订阅)
     */
    public static final String EVENT_TYPE_UNSUBSCRIBE = "unsubscribe";

    /**
     * 事件类型：CLICK(自定义菜单点击事件)
     */
    public static final String EVENT_TYPE_CLICK = "CLICK";


    /**
     * 根据发送信息构造返回的xml
     *
     * @param receivedMsg
     * @param replyMsg
     * @return
     */
    public static String createTextRspMsg(TextReqMsg receivedMsg, String replyMsg) {
        TextRspMsg textRspMsg = new TextRspMsg();
        textRspMsg.setContent(replyMsg);
        textRspMsg.setCreateTime(DateUtils.getCurrentDate());
        textRspMsg.setMessageType(receivedMsg.getMessageType());
        //此处注意发送和接收者对调
        textRspMsg.setToUserName(receivedMsg.getFromUserName());
        textRspMsg.setFromUserName(receivedMsg.getToUserName());
        return XmlUtil.toXml(textRspMsg);
    }

    public static void main(String[] args) {
        TextRspMsg textRspMsg = new TextRspMsg();
        String replyMsg = "sdsdsd";

        textRspMsg.setContent(replyMsg);
        System.out.println(XmlUtil.toXml(textRspMsg));
    }

    /**
     * 返回图片消息体
     *
     * @param imgReqMsg
     * @return
     */
    public static String createImgRspMsg(ImgReqMsg imgReqMsg) {
        ImgRspMsg imgRspMsg = new ImgRspMsg();
        imgRspMsg.setImage(new Image(imgReqMsg.getMediaId()));
        imgRspMsg.setCreateTime(imgReqMsg.getCreateTime());
        imgRspMsg.setMessageType(MessageUtil.REQ_MESSAGE_TYPE_IMAGE);
        imgRspMsg.setFromUserName(imgReqMsg.getToUserName());
        imgRspMsg.setToUserName(imgReqMsg.getFromUserName());
        return XmlUtil.toXml(imgRspMsg);
    }

    /**
     * 回复语言消息
     *
     * @param voiceReqMsg
     * @return
     */
    public static String createVoiceRspMsg(VoiceReqMsg voiceReqMsg) {
        VoiceRsqMsg voiceRspMsg = new VoiceRsqMsg();
        voiceRspMsg.setFormat(voiceReqMsg.getFormat());
        voiceRspMsg.setVoice(new Voice(voiceReqMsg.getMediaId()));
        voiceRspMsg.setCreateTime(voiceReqMsg.getCreateTime());
        voiceRspMsg.setMessageType(MessageUtil.REQ_MESSAGE_TYPE_VOICE);
        voiceRspMsg.setFromUserName(voiceReqMsg.getToUserName());
        voiceRspMsg.setToUserName(voiceReqMsg.getFromUserName());
        return XmlUtil.toXml(voiceRspMsg);
    }

    /**
     * 根据事件类型组装回复消息体
     *
     * @param eventType
     * @return
     */
    public static String createEventRspMsg(String eventType, String fromUser, String toUser, String crtateTime) {
        String eventRspMsg = "";
        TextRspMsg textRspMsg = null;
        switch (eventType) {
            /**
             * 关注事件自动回复内容
             */
            case MessageUtil.EVENT_TYPE_SUBSCRIBE:
                eventRspMsg = "/:rose/:rose亲，欢迎光临！！！小智在此恭候多时/:jump，O(∩_∩)O谢谢你的关注。/:<L>";
                textRspMsg = new TextRspMsg(toUser, fromUser, crtateTime, MessageUtil.REQ_MESSAGE_TYPE_TEXT, eventRspMsg);
                break;
            /**
             * 取消关注事件自动回复内容
             */
            case MessageUtil.EVENT_TYPE_UNSUBSCRIBE:
                eventRspMsg = "/:8*/:8*亲，您真的忍心就这样走了么？小智在这里等着你回来/:hug";
                textRspMsg = new TextRspMsg(toUser, fromUser, crtateTime, MessageUtil.REQ_MESSAGE_TYPE_TEXT, eventRspMsg);
                break;
            // TODO 后续添加更多其他的时间处理
        }
        return XmlUtil.toXml(textRspMsg);
    }

}
