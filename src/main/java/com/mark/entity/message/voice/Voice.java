/**
 * @file Voice.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.voice;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 语音消息
 * @author maliqiang
 * @create 2017-08-03
 * @version 1.0
 */
public class Voice {
    /**
     * 多媒体资源ID，多媒体消息才有
     */
    @XStreamAlias("MediaId")
    private String mediaId;

    public Voice() {
    }

    public Voice(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
