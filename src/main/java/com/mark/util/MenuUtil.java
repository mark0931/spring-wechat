package com.mark.util;

import com.alibaba.fastjson.JSON;
import com.mark.constant.ButtonType;
import com.mark.entity.menu.Button;
import com.mark.entity.menu.Menu;
import com.mark.entity.menu.SubButton;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MenuUtil {
	private static String access_token= AccessTokenUtils.getOrRefreshAccessToken();

	 /**
	  * 创建Menu
	 * @Title: createMenu
	 * @Description: 创建Menu
	 * @param @return
	 * @param @throws IOException    设定文件
	 * @return int    返回类型
	 * @throws
	  */
	    public static String createMenu(String menuJson,String accessToken) {
	    //String menu = "{\"button\":[{\"type\":\"click\",\"name\":\"MENU01\",\"key\":\"1\"},{\"type\":\"click\",\"name\":\"天气查询\",\"key\":\"西安\"},{\"name\":\"日常工作\",\"sub_button\":[{\"type\":\"click\",\"name\":\"待办工单\",\"key\":\"01_WAITING\"},{\"type\":\"click\",\"name\":\"已办工单\",\"key\":\"02_FINISH\"},{\"type\":\"click\",\"name\":\"我的工单\",\"key\":\"03_MYJOB\"},{\"type\":\"click\",\"name\":\"公告消息箱\",\"key\":\"04_MESSAGEBOX\"},{\"type\":\"click\",\"name\":\"签到\",\"key\":\"05_SIGN\"}]}]}";
			//此处改为自己想要的结构体，替换即可
//	        String access_token=AccessTokenUtils.getAccessToken();  //"PDwv9s24mOOF-Sl2jupmEQerPW632zLLoDhDcCxeU4shfqBLhtLp6rMSo-_BlqNkkj8IjIxvaQL1H2HJRzXEu33n-m2cKdtywf9bRgIbhjugk5xJnAhPjrblz9-QRB3qHXKbAGASIV";
	        String action = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+accessToken;
	        try {
	           URL url = new URL(action);
	           HttpURLConnection http =   (HttpURLConnection) url.openConnection();    

	           http.setRequestMethod("POST");        
	           http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");    
	           http.setDoOutput(true);        
	           http.setDoInput(true);
	           System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
	           System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
	           http.connect();
	           OutputStream os= http.getOutputStream();    
	           os.write(menuJson.getBytes("UTF-8"));//传入参数
	           os.flush();
	           os.close();

	           InputStream is =http.getInputStream();
	           int size =is.available();
	           byte[] jsonBytes =new byte[size];
	           is.read(jsonBytes);
	           String message=new String(jsonBytes,"UTF-8");
	           return "返回信息"+message;
	           } catch (MalformedURLException e) {
	               e.printStackTrace();
	           } catch (IOException e) {
	               e.printStackTrace();
	           }    
	        return "createMenu 失败";
	   }
     
	 /**
	  * 构建菜单
	  * @return
	  */
	 private static String bulidMenue() {
		StringBuffer buf = new StringBuffer();
		buf.append("{\"button\":[ ")
		    //第一个菜单项
		   .append("{ \"name\":\"云农e家\"")
//				   "\"sub_button\":[{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"淘宝便利店\",")
//		   .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//		   .append("},{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"微信小店\",")
//		   .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//		   .append("},{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"微商代理\",")
//		   .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
		   .append("}]}")
		   //构建第二个菜单项
//		   .append("{ \"name\":\"云农部落\", \"sub_button\":[{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"部落文明\",")
//		   .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//		   .append("},{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"部落发明\",")
//		   .append("\"url\":\"http://game.html5youxi.com/201501/ljsp/#rd\"")
//		   .append("},{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"相亲大会\",")
//		   .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//		   .append("}]},")
//		   //构建第三个菜单项目
//		    .append("{ \"name\":\"云农生态1\", \"sub_button\":[{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"甜过初恋\",")
//		   .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
////		   .append("},{")
//		   .append("\"type\":\"view\",")
//		   .append("\"name\":\"云农计划\",")
//				 .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//				 .append("},{")
//				 .append("\"type\":\"view\",")
//				 .append("\"name\":\"云农软件园\",")
//				 .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//				 .append("},{")
//				 .append("\"type\":\"view\",")
//				 .append("\"name\":\"云农服务\",")
//				 .append("\"url\":\"http://m.yncountry.icoc.in/index.jsp\"")
//				 .append("},{")
//				 .append("\"type\":\"click\",")
//				 .append("\"name\":\"在线客服\",")
//				 .append("\"key\":\"V1001_TODAY_KEFU\"")
//				 .append("}]}")
				 .append("]}");
		return buf.toString();
	}
	 
	 public static void main(String[] args) {
//		String bulidMenue = bulidMenue();
		 Menu menu = new Menu();

		 List<Button> btns = new ArrayList<>();
		 /**
		  * 一级菜单
		  */
		 Button button = new Button();
		 button.setName("我的助手");
		 button.setType(ButtonType.CLICK.getTypeName());
		 button.setKey("helper");

		 List<SubButton> list = new ArrayList<>();
		 /**
		  * 二级菜单
		  */
		 SubButton subButton = new SubButton();
		 subButton.setKey("11");
		 subButton.setName("查天气");
		 subButton.setType(ButtonType.VIEW.getTypeName());
		 subButton.setUrl("http://www.weather.com.cn/live/");
		 list.add(subButton);
		 button.setSub_button(list);

		 btns.add(button);
		 menu.setButton(btns);

		 String bulidMenue = JSON.toJSONString(menu);
		 System.out.println("token:"+access_token);
		 /**
		  * 创建菜单
		  */
		 String res = createMenu(bulidMenue,access_token);
		System.out.println(bulidMenue+","+res);
	}

		/**
	     * 删除当前Menu
	    * @Title: deleteMenu
	    * @Description: 删除当前Menu
	    * @param @return    设定文件
	    * @return String    返回类型
	    * @throws
	     */
	   public static String deleteMenu()
	   {

	       String action = HttpUtil.BASE_URL+"menu/delete? access_token="+access_token;
	       try {
	          URL url = new URL(action);
	          HttpURLConnection http =   (HttpURLConnection) url.openConnection();    

	          http.setRequestMethod("GET");        
	          http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");    
	          http.setDoOutput(true);        
	          http.setDoInput(true);
	          System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
	          System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
	          http.connect();
	          OutputStream os= http.getOutputStream();    
	          os.flush();
	          os.close();

	          InputStream is =http.getInputStream();
	          int size =is.available();
	          byte[] jsonBytes =new byte[size];
	          is.read(jsonBytes);
	          String message=new String(jsonBytes,"UTF-8");
	          return "deleteMenu返回信息:"+message;
	          } catch (MalformedURLException e) {
	              e.printStackTrace();
	          } catch (IOException e) {
	              e.printStackTrace();
	          }
	       return "deleteMenu 失败";   
	   }
	
}
