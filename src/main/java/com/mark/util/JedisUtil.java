/**
 * @file JedisUtil.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.util;

import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * redis操作工具类
 *
 * @author maliqiang
 * @version 1.0
 * @create 2017-06-12
 */
public class JedisUtil {
    @Autowired
    private JedisPool jedisPool;

    public static Jedis jedis = null;
    /**
     * 获取redis实例
     * @return
     */
    public synchronized Jedis getInstance() {
        jedis = jedisPool.getResource();
        return jedis;
    }

    public void incr(String key){
        jedis.incr(key);
    }

    public void set(String key,String value){
        jedis.set(key,value);
    }

}
