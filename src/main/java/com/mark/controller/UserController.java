/**
 * @file UserController.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.controller;

import com.mark.entity.User;
import com.mark.service.UserService;
import com.mark.util.EncryptUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author maliqiang
 * @version 1.0
 * @create 2017-06-22
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private static Logger log = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService userService;

    /**
     * 用户登录
     *
     * @return
     */
    @RequestMapping("/login")
    public String login(User user, HttpServletRequest request) {
        String page = null;
        log.info("用户登录...");
        User u = userService.getByUserName(user.getUserName());
        Integer len = u.getIdNo().length();
        String inputPass = EncryptUtil.md5(user.getPassword().trim()) + u.getIdNo().substring(len - 4, len);
        if (EncryptUtil.md5(inputPass).equals(u.getPassword().trim())) {
            user.setPassword(null);//防止密码暴露
            HttpSession session = request.getSession();
            session.setAttribute("u", u);
            page = "index";
        } else {
            log.error("用户名或密码错误！");

//            return "";
        }
        return page;
        //TODO  实现页面部分参数数据库可配置化：比如项目名称，版权声明等。
    }

    @RequestMapping("/loginout")
    public String loginOut(User user, HttpServletRequest request) {
        log.info("用户退出...");
        HttpSession session = request.getSession();
        session.invalidate();
        return "tologin";
        //TODO  实现页面部分参数数据库可配置化：比如项目名称，版权声明等。
    }

}
