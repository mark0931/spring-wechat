/**
 * @file ConstantValue.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.constant;

/**
 * 常量
 * @author maliqiang
 * @create 2017-07-26
 * @version 1.0
 */
public class ConstantValue {
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String TOKEN = "mark";
}
