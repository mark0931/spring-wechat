/**
 * @file VoiceReqMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.voice;

import com.mark.entity.message.RequestBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 音频消息体
 * @author maliqiang
 * @create 2017-07-30
 * @version 1.0
 */
@XStreamAlias("xml")
public class VoiceReqMsg extends RequestBaseMessage {

    /**
     * 语言消息格式，如amr，speex等
     */
    @XStreamAlias("Format")
    private String format;

    /**
     * 语音识别结果，UTF8编码
     */
    @XStreamAlias("Recognition")
    private String recognition;
    /**
     * 多媒体资源ID，多媒体消息才有
     */
    @XStreamAlias("MediaId")
    private String mediaId;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getRecognition() {
        return recognition;
    }

    public void setRecognition(String recognition) {
        this.recognition = recognition;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
