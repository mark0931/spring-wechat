/**
 * @file RestPwdJob.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 密码重置job任务
 * @author maliqiang
 * @create 2017-07-04
 * @version 1.0
 */
public class RestPwdJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //TODO  此处实现定时的密码更新，防止演示项目有人修改密码
    }
}
