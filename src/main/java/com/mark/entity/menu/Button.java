/**
 * @file Button.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.menu;

import java.util.List;

/**
 * 菜单目录
 * @author maliqiang
 * @create 2017-07-23
 * @version 1.0
 */
public class Button {
    /**
     * 菜单标题，不超过16个字节，子菜单不超过60个字节,必填
     */
    private String name;
    /**
     * 菜单类型：必填
     */
    private String type;
    /**
     * 一级菜单key值：click必填
     */
    private String key;

    /**
     * 二级菜单
     */
    private List<SubButton> sub_button;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubButton> getSub_button() {
        return sub_button;
    }

    public void setSub_button(List<SubButton> sub_button) {
        this.sub_button = sub_button;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
