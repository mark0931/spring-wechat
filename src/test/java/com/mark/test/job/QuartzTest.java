package com.mark.test.job;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class QuartzTest {

    public static void main(String[] args) throws JobExecutionException {

        try {
            // Grab the Scheduler instance from the Factory
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // define the job and tie it to our HelloJob class
            JobDetail job = newJob(HelloJob.class)
                    .withIdentity("job1", "group1")
                    .build();

            // Trigger the job to run now, and then repeat every 40 seconds
            Trigger trigger = newTrigger()
                    .withIdentity("trigger1", "group1")
//                    .startNow()//立即启动
                    .startAt(DateBuilder.futureDate(5, DateBuilder.IntervalUnit.SECOND))//5秒后启动
                    .withSchedule(simpleSchedule()
                            .withIntervalInSeconds(2)//多少秒执行一次
                            .repeatForever())//一直执行
                    .build();

            // Tell quartz to schedule the job using our trigger
            scheduler.scheduleJob(job, trigger);
            // and start it off
            scheduler.start();
//            scheduler.shutdown();

        } catch (Exception e) {
            JobExecutionException e2 =
                    new JobExecutionException(e);

            // 1、this job will refire immediately
            e2.refireImmediately();

            // 2、Quartz will automatically unschedule
            // all triggers associated with this job
            // so that it does not run again
            e2.setUnscheduleAllTriggers(true);
            throw e2;
        }
    }
}