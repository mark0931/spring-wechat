/**
 * @file ImgReqMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.img;

import com.mark.entity.message.RequestBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 图片请求消息体
 * @author maliqiang
 * @create 2017-07-30
 * @version 1.0
 */
@XStreamAlias("xml")
public class ImgReqMsg extends RequestBaseMessage {
    @XStreamAlias("PicUrl")
    private String picUrl;//图片URL，图片消息才有
    @XStreamAlias("MediaId")
    private String mediaId;//多媒体资源ID，多媒体消息才有


    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
