/**
 * @file TestLogUtil.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.test.log;

import com.mark.util.LogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author maliqiang
 * @create 2017-06-25
 * @version 1.0
 */
public class TestLogUtil {
    public static org.apache.log4j.Logger logger1 = org.apache.log4j.Logger.getLogger(TestLogUtil.class);
    public static Logger logger = LoggerFactory.getLogger(TestLogUtil.class);
    public static void main(String[] args) {
        logger1.info("测试log4j");
        logger.info("测试开始...");
    }
}
