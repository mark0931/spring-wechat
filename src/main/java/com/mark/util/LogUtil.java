/**
 * @file LogUtil.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志工具类
 * @author maliqiang
 * @create 2017-06-25
 * @version 1.0
 */
public class LogUtil {

    private static Logger log = null;

    public static  void debug(Class clz,String debugMsg){
        log = LoggerFactory.getLogger(clz);
        log.debug(debugMsg);
    }
    public static  void warn(Class clz,String warnMsg){
        log = LoggerFactory.getLogger(clz);
        log.warn(warnMsg);
    }
    public static  void info(Class clz,String info){
        log = LoggerFactory.getLogger(clz);
        log.info(info);
    }

    public static  void errror(Class clz,String errInfo){
        log = LoggerFactory.getLogger(clz);
        log.error(errInfo);
    }
}
