/**
 * @file ClickButton.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.menu;

/**
 * click事件按钮
 * @author maliqiang
 * @create 2017-07-24
 * @version 1.0
 */
public class ClickButton extends Button {
    /**
     * click等点击类型必须:菜单KEY值，用于消息接口推送，不超过128字节
     */
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
