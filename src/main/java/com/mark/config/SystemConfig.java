/**
 * @file SystemConfig.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 系统参数配置读取
 * @author maliqiang
 * @create 2017-06-24
 * @version 1.0
 */
@Component
public class SystemConfig {
    @Value("sys_name")
    private String systemName;
}
