/**
 * @file ButtonType.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.constant;

/**
 * button类型枚举类
 * @author maliqiang
 * @create 2017-07-24
 * @version 1.0
 */
public enum  ButtonType {

    VIEW("view",1,"网页类型:跳转URL用户点击view类型按钮后，微信客户端将会打开开发者在按钮中填写的网页URL，可与网页授权获取用户基本信息接口结合，获得用户基本信息。"),
    CLICK("click",2,"点击类型"),
    MINI_PROGRAM("miniprogram",3,"小程序类型"),
    SCANCODE_PUSH("scancode_push",4,"扫码推事件用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息"),
    SCANCODE_WAITMSG("scancode_waitmsg",5,"扫码推事件且弹出“消息接收中”提示框用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息"),
    PIC_SYSPHOTO("pic_sysphoto",6,"弹出系统拍照发图用户点击按钮后，微信客户端将调起系统相机，完成拍照操作后，会将拍摄的相片发送给开发者，并推送事件给开发者，同时收起系统相机，随后可能会收到开发者下发的消息。"),
    PIC_PHOTO_OR_ALBUM("pic_photo_or_album",7,"弹出拍照或者相册发图用户点击按钮后，微信客户端将弹出选择器供用户选择“拍照”或者“从手机相册选择”。用户选择后即走其他两种流程。"),
    PIC_WEIXIN("pic_weixin",8,"弹出微信相册发图器用户点击按钮后，微信客户端将调起微信相册，完成选择操作后，将选择的相片发送给开发者的服务器，并推送事件给开发者，同时收起相册，随后可能会收到开发者下发的消息。"),
    LOCATION_SELECT("location_select",9,"弹出地理位置选择器用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，随后可能会收到开发者下发的消息"),
    MEDIA_ID("media_id",10,""),
    VIEW_LIMITED("view_limited",11,"跳转图文消息URL用户点击view_limited类型按钮后，微信客户端将打开开发者在按钮中填写的永久素材id对应的图文消息URL，永久素材类型只支持图文消息。请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。");

    ButtonType(String typeName, Integer typeId, String desc) {
        this.typeName = typeName;
        this.typeId = typeId;
        this.desc = desc;
    }

    /**
     * 类型名称
     */
    private String typeName;
    /**
     * 类型编号
     */
    private Integer typeId;

    private String desc;


    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
