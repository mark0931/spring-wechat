/**
 * @file SubButton.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.menu;

/**
 * 二级菜单
 * @author maliqiang
 * @create 2017-07-23
 * @version 1.0
 */
public class SubButton extends Button{
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
