/**
 * @file AccessToken.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity;

/**
 * access_token实体类
 * @author maliqiang
 * @create 2017-07-23
 * @version 1.0
 */
public class AccessToken {
    /**
     * 访问权限token
     */
    private String access_token;
    /**
     * 有效期，单位秒
     */
    private String expires_in;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }
}
