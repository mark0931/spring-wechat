package com.mark.entity.message.text;

import com.mark.entity.message.ResponseBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("xml")
public class TextRspMsg extends ResponseBaseMessage {

	@XStreamAlias("Content")
	private String content;// 内容


	public TextRspMsg() {
	}

	public TextRspMsg(String fromUserName, String toUserName, String createTime, String messageType, String content) {
		super(fromUserName, toUserName, createTime, messageType);
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
