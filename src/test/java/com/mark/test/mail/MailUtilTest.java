package com.mark.test.mail;

import com.mark.util.mail.MailUtil;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MailUtilTest {
    private static Logger log = LoggerFactory.getLogger(MailUtilTest.class);
 
    @Test
    public void testMailTemplate() {
        String templateName = "mail.ftl";
        Map<String, String> map = new HashMap<String, String>();
        map.put("content", "test");
        try {
            MailUtil.sendMailByTemplate("maliqiang@yazuo.com", "test", map,
                    templateName);
        } catch (IOException e) {
            log.error(e.toString(), e);
        } catch (TemplateException e) {
            log.error(e.toString(), e);
        } catch (MessagingException e) {
            log.error(e.toString(), e);
        }
    }
 
    @Test
    public void testMail() {
        try {
            MailUtil.sendMail("maliqiang@yazuo.com", "test", "普通邮件");
        } catch (IOException e) {
            log.error(e.toString(), e);
        } catch (MessagingException e) {
            log.error(e.toString(), e);
        }
    }
 
    @Test
    public void testMailAndFile() {
        try {
            String filePath = "d:/data.zip";
            MailUtil.sendMailAndFile("maliqiang@yazuo.com", "test", filePath,"普通邮件"
                    );
        } catch (IOException e) {
            log.error(e.toString(), e);
        } catch (MessagingException e) {
            log.error(e.toString(), e);
        }
    }
}
