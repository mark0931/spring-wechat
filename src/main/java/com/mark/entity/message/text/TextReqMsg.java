/**
 * @file TextReqMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.text;

import com.mark.entity.message.RequestBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 普通文本消息请求体
 * @author maliqiang
 * @create 2017-07-30
 * @version 1.0
 */
@XStreamAlias("xml")
public class TextReqMsg extends RequestBaseMessage {
    @XStreamAlias("Content")
    private String content;// 内容

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
