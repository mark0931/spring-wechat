/**
 * @file URL.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.constant;

import com.mark.util.ConfigManager;

/**
 * url常量类
 * @author maliqiang
 * @create 2017-07-25
 * @version 1.0
 */
public class URL {
    public static final String BASE_URL = ConfigManager.getString("base_url","https://sh.api.weixin.qq.com/cgi-bin/");

    public static final String ACCESS_TOKEN_URL = "";

}
