/**
 * @file HelloJob.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.test.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * HelloJob is a simple job
 * that implements the Job interface and logs a nice message to the log
 * @author maliqiang
 * @create 2017-06-10
 * @version 1.0
 */
public class HelloJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("quartz test starting...");
    }
}
