/**
 * @file TestWriteProperties.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.test;

import com.mark.constant.ConstantValue;
import com.mark.entity.User;
import com.mark.util.AccessTokenUtils;
import com.mark.util.ConfigManager;
import org.junit.Test;

/**
 * properties文件读写测试
 * @author maliqiang
 * @create 2017-08-01
 * @version 1.0
 */
public class TestWriteProperties {
    @Test
    public void testWrite(){
        String access_token = "test123";
        boolean result = ConfigManager.writeProperties(ConstantValue.ACCESS_TOKEN,access_token,null);
    }

}
