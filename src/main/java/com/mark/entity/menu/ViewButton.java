/**
 * @file ViewButton.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.menu;

/**
 * view事件按钮
 * @author maliqiang
 * @create 2017-07-24
 * @version 1.0
 */
public class ViewButton extends Button{
    /**
     * view、miniprogram类型必须:网页链接，用户点击菜单可打开链接，不超过1024字节。type为miniprogram时，不支持小程序的老版本客户端将打开本url。
     */
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
