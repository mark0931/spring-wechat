/**
 * @file ImgRspMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.img;

import com.mark.entity.message.ResponseBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 图片响应消息体
 * @author maliqiang
 * @create 2017-07-30
 * @version 1.0
 */
@XStreamAlias("xml")
public class ImgRspMsg extends ResponseBaseMessage {
    @XStreamAlias("Image")
    private Image image;//图片资源信息
//    @XStreamAlias("Format")
//    private String format;//语言消息格式，如amr，speex等

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
