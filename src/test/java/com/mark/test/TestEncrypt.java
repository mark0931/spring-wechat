/**
 * @file TestEncrypt.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.test;

import com.mark.util.MacUtils;
import org.junit.Test;

/**
 *
 * @author maliqiang
 * @create 2017-07-22
 * @version 1.0
 */
public class TestEncrypt {
    @Test
    public void encrypt(){
        String key = "#mark_wx";
        String appid = "wx0dc82ef004c89b1c";
        String scerect = "6c87536a6ae5fccf5fba1728b244b368";
        String appidStr = MacUtils.encrypt(key,appid);
        String scerectStr = MacUtils.encrypt(key,scerect);

        System.out.println(appidStr+",\n"+scerectStr);
    }
}
