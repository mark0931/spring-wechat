/**
 * @file UserServiceImpl.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.service.impl;

import com.mark.dao.UserDao;
import com.mark.entity.User;
import com.mark.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * @author maliqiang
 * @create 2017-06-25
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public int insert(User pojo) {
        return userDao.insert(pojo);
    }

    @Override
    public int insertSelective(User pojo) {
        return userDao.insertSelective(pojo);
    }

    @Override
    public int insertList(List<User> pojos) {
        return userDao.insertList(pojos);
    }

    @Override
    public int update(User pojo) {
        return userDao.update(pojo);
    }

    @Override
    public User getByUserName(String userName) {
        return userDao.getByUserName(userName);
    }
}
