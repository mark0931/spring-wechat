/**
 * @file Menu.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.menu;

/**
 * menu菜单栏
 * @author maliqiang
 * @create 2017-07-23
 * @version 1.0
 */

import java.util.List;

/**
 * 请注意：
 1、自定义菜单最多包括3个一级菜单，每个一级菜单最多包含5个二级菜单。
 2、一级菜单最多4个汉字，二级菜单最多7个汉字，多出来的部分将会以“...”代替。
 3、创建自定义菜单后，菜单的刷新策略是，在用户进入公众号会话页或公众号profile页时，如果发现上一次拉取菜单的请求在5分钟以前，
 就会拉取一下菜单，如果菜单有更新，就会刷新客户端的菜单。
 测试时可以尝试取消关注公众账号后再次关注，则可以看到创建后的效果。
 */
public class Menu {
    private List<Button> button;

    public List<Button> getButton() {
        return button;
    }

    public void setButton(List<Button> button) {
        this.button = button;
    }
}
