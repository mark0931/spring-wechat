/**
 * @file EventReqMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.event;

import com.mark.entity.message.RequestBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 事件消息请求体
 * @author maliqiang
 * @create 2017-08-03
 * @version 1.0
 */
public class EventReqMsg extends RequestBaseMessage{
    @XStreamAlias("Event")
    private String event;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
