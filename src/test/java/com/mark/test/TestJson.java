/**
 * @file TestJson.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 *
 * @author maliqiang
 * @create 2017-07-03
 * @version 1.0
 */
public class TestJson {
    public static void main(String[] args) {
        String json = "{\"biz_no\":\"ZM201707033000000761200355118689\",\"success\":true,\"vars\":[{\"key\":\"activity_area_stability\",\"value\":\"02\"},{\"key\":\"have_car_flag\",\"value\":\"02\"},{\"key\":\"have_fang_flag\",\"value\":\"02\"},{\"key\":\"auth_fin_last_6m_cnt\",\"value\":\"04\"},{\"key\":\"auth_fin_last_3m_cnt\",\"value\":\"04\"},{\"key\":\"auth_fin_last_1m_cnt\",\"value\":\"03\"},{\"key\":\"credit_pay_amt_1y\",\"value\":\"04\"},{\"key\":\"credit_pay_amt_3m\",\"value\":\"04\"},{\"key\":\"credit_pay_amt_1m\",\"value\":\"04\"},{\"key\":\"credit_pay_months_1y\",\"value\":\"10\"},{\"key\":\"credit_total_pay_months\",\"value\":\"08\"},{\"key\":\"credit_duration\",\"value\":\"04\"},{\"key\":\"last_1y_avg_asset_total\",\"value\":\"06\"},{\"key\":\"last_3m_avg_asset_total\",\"value\":\"05\"},{\"key\":\"last_1m_avg_asset_total\",\"value\":\"03\"},{\"key\":\"tot_pay_amt_3m\",\"value\":\"02\"},{\"key\":\"tot_pay_amt_1m\",\"value\":\"03\"},{\"key\":\"ebill_pay_amt_6m\",\"value\":\"04\"},{\"key\":\"ebill_pay_amt_3m\",\"value\":\"04\"},{\"key\":\"ebill_pay_amt_1m\",\"value\":\"05\"},{\"key\":\"avg_puc_sdm_last_1y\",\"value\":\"01\"},{\"key\":\"pre_1y_pay_cnt\",\"value\":\"05\"},{\"key\":\"pre_1y_pay_amount\",\"value\":\"06\"},{\"key\":\"relevant_stability\",\"value\":\"03\"},{\"key\":\"sns_pii\",\"value\":\"06\"},{\"key\":\"ovd_order_cnt_2y_m3_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_2y_m6_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_6m\",\"value\":\"01\"},{\"key\":\"ovd_order_amt_6m\",\"value\":\"01\"},{\"key\":\"ovd_order_cnt_6m_m1_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_3m\",\"value\":\"01\"},{\"key\":\"ovd_order_amt_3m\",\"value\":\"01\"},{\"key\":\"ovd_order_cnt_3m_m1_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_5y_m3_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_5y_m6_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_1m\",\"value\":\"01\"},{\"key\":\"ovd_order_amt_1m\",\"value\":\"01\"},{\"key\":\"ovd_order_cnt_12m\",\"value\":\"01\"},{\"key\":\"ovd_order_amt_12m\",\"value\":\"01\"},{\"key\":\"ovd_order_cnt_12m_m1_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_12m_m3_status\",\"value\":\"N\"},{\"key\":\"ovd_order_cnt_12m_m6_status\",\"value\":\"N\"},{\"key\":\"occupation\",\"value\":\"无法识别\"},{\"key\":\"company_name\",\"value\":\"陕西博盛汇通汽车租赁有限公司(外部数据)\"},{\"key\":\"consume_steady_byxs_1y\",\"value\":\"07\"},{\"key\":\"use_mobile_2_cnt_1y\",\"value\":\"01\"}]}\n" +
                "\n" +
                "\n";
        JSONObject obj = JSON.parseObject(json);
        System.out.println(obj.get("consume_steady_byxs_1y"));
    }
}
