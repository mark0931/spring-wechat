package com.mark.entity.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
public class RequestBaseMessage {
	@XStreamAlias("ToUserName")
	private String toUserName;// 接收人:开发者微信号
	@XStreamAlias("FromUserName")
	private String fromUserName;// 发送人:用户OpenId
	@XStreamAlias("CreateTime")
	private String createTime;// 创建日期
	@XStreamAlias("MsgType")
	private String messageType;// 消息类型
	@XStreamAlias("MsgId")
	private String msgId;// 消息编号


	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "RequestBaseMessage{" +
				"toUserName='" + toUserName + '\'' +
				", fromUserName='" + fromUserName + '\'' +
				", createTime='" + createTime + '\'' +
				", messageType='" + messageType + '\'' +
				", msgId='" + msgId + '\'' +
				'}';
	}
}
