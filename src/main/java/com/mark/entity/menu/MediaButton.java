/**
 * @file MediaButton.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.menu;

/**
 * media_id类型和view_limited类型
 * @author maliqiang
 * @create 2017-07-24
 * @version 1.0
 */
public class MediaButton extends Button {
    /**
     * media_id类型和view_limited类型必须：调用新增永久素材接口返回的合法media_id
     */
    private String media_id;
    /**
     * miniprogram类型必须：小程序的appid（仅认证公众号可配置）
     */
    private String appid;
    /**
     * miniprogram类型必须:小程序的页面路径
     */
    private String pagepath;

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPagepath() {
        return pagepath;
    }

    public void setPagepath(String pagepath) {
        this.pagepath = pagepath;
    }
}
