/**
 * @file VoiceReqMsg.java
 * @project spring-wechat
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.entity.message.voice;

import com.mark.entity.message.RequestBaseMessage;
import com.mark.entity.message.ResponseBaseMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 音频消息体
 * @author maliqiang
 * @create 2017-07-30
 * @version 1.0
 */
@XStreamAlias("xml")
public class VoiceRsqMsg extends ResponseBaseMessage {

    /**
     * 语言消息格式，如amr，speex等
     */
    @XStreamAlias("Format")
    private String format;
    @XStreamAlias("Voice")
    private Voice voice;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }
}
