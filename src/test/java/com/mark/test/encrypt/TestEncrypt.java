/**
 * @file TestEncrypt.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.test.encrypt;

import com.mark.util.EncryptUtil;
import org.junit.Test;

/**
 * 加密单元测试
 * @author maliqiang
 * @create 2017-06-25
 * @version 1.0
 */
public class TestEncrypt {
    @Test
   public void md5(){
       String idNo = "620523199108088888";
       String name = "mark";
        System.out.println(EncryptUtil.md5(EncryptUtil.md5(name)+idNo.substring(idNo.length()-4,idNo.length())));
   }
}
