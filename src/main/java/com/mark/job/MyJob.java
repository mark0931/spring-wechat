/**
 * @file MyJob.java
 * @project simple-SpringMVC
 * @copyright 无锡雅座在线科技发展有限公司
 */
package com.mark.job;

import com.mark.constant.ConstantValue;
import com.mark.util.AccessTokenUtils;
import com.mark.util.ConfigManager;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * 刷新token的定时任务
 * @author maliqiang
 * @create 2017-06-10
 * @version 1.0
 */
public class MyJob implements Job {
    private Logger logger = LoggerFactory.getLogger(MyJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("定时任务启动"+new Date());
        String access_token = AccessTokenUtils.getOrRefreshAccessToken();
        boolean result = ConfigManager.writeProperties(ConstantValue.ACCESS_TOKEN,access_token,null);
        if(result){
            logger.info("token获取（刷新）成功！");
        }else {
            logger.error("token获取失败。请重试！");
        }
    }
}
